export interface IAZContact {
    firstName?: string;
    lastName?: string;
    telephoneNumber?: string;
}

export interface IContact{
    territoryNumber?: number;
    fullName?: string;
    address?: string;
    address2?: string;
    city?: string;
    state?: string;
    zip?: string; 
    mailingAddress?: string;
    mailingAddress2?: string;
    mailingCity?: string;
    mailingState?: string;
    mailingZip?: string;
    notes?: string;
    updatedOn?: Date;
    possibleRental?: Boolean;
    azData?: Array<IAZContact>;
    colesData?: Array<IColesContact>;
  }

  export interface IColesContact{
     lastName: string;
     firstName: string;
     houseNumber: string;
     appartment?: string;
     street: string;
     city?: string;
     state?: string;
     zip?: string; 
     homePhoneCode?: string;
     homePhoneNumber?: string;
     cellPhoneCode?: string;
     cellPhoneNumber?: string;
     emailAddress?: string;
  }

  export class ITerritory {
    _id?: number; //configured
    type?: string; //Normal //MobileHomes //Apartments
    name?: string; //configured
    streetNames?: string[]; //configured
    streets?: Array<{name: string, alternateName?: string, parity?:"ODD"|"EVEN"|"BOTH", startNumber?:Number, endNumber?:Number}>
    alternativeNames?: Array<{ name: string, alternate: string }>;
    phoneContacts?: Array<IContact>;
    letterContacts?: Array<IContact>;
    doNotCalls?: Array<IContact>;
    archivePhoneContacts?: Array<IContact>;
    archiveLetterContacts?: Array<IContact>;
    archiveDoNotCalls?: Array<IContact>;
    lastUpdated?: Date;
}
